# Spring Boot Store Procedure (MySQL)
An example on how to create & run store procedure using Spring Boot

## Things to do list:
1. Clone this repository: `git clone https://gitlab.com/hendisantika/spring-boot-procedure.git`.
2. Go to folder: `cd spring-boot-procedure`.
3. Run the application: `mvn clean spring-boot:run`.


## Console Log
```shell script
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.2.6.RELEASE)

2020-04-07 15:51:01.214  WARN 55944 --- [           main] o.s.boot.StartupInfoLogger               : InetAddress.getLocalHost().getHostName() took 5016 milliseconds to respond. Please verify your network configuration (macOS machines may need to add entries to /etc/hosts).
2020-04-07 15:51:01.217  INFO 55944 --- [           main] c.h.p.SpringBootProcedureApplication     : Starting SpringBootProcedureApplication on Hendis-MacBook-Pro.local with PID 55944 (/Users/hendisantika/Documents/IdeaProjects/spring-boot-procedure/target/classes started by hendisantika in /Users/hendisantika/Documents/IdeaProjects/spring-boot-procedure)
2020-04-07 15:51:01.218  INFO 55944 --- [           main] c.h.p.SpringBootProcedureApplication     : No active profile set, falling back to default profiles: default
2020-04-07 15:51:01.626  INFO 55944 --- [           main] .s.d.r.c.RepositoryConfigurationDelegate : Bootstrapping Spring Data JPA repositories in DEFAULT mode.
2020-04-07 15:51:01.676  INFO 55944 --- [           main] .s.d.r.c.RepositoryConfigurationDelegate : Finished Spring Data repository scanning in 41ms. Found 1 JPA repository interfaces.
2020-04-07 15:51:02.054  INFO 55944 --- [           main] o.f.c.internal.license.VersionPrinter    : Flyway Community Edition 6.3.2 by Redgate
2020-04-07 15:51:02.059  INFO 55944 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Starting...
2020-04-07 15:51:02.217  INFO 55944 --- [           main] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Start completed.
2020-04-07 15:51:02.222  INFO 55944 --- [           main] o.f.c.internal.database.DatabaseFactory  : Database: jdbc:mysql://localhost:3306/procedureDB (MySQL 5.5)
2020-04-07 15:51:02.276  INFO 55944 --- [           main] o.f.core.internal.command.DbValidate     : Successfully validated 2 migrations (execution time 00:00.021s)
2020-04-07 15:51:02.316  INFO 55944 --- [           main] o.f.c.i.s.JdbcTableSchemaHistory         : Creating Schema History table `procedureDB`.`flyway_schema_history` ...
2020-04-07 15:51:02.377  WARN 55944 --- [           main] o.f.c.i.s.DefaultSqlScriptExecutor       : DB: Name 'flyway_schema_history_pk' ignored for PRIMARY key. (SQL State: 42000 - Error Code: 1280)
2020-04-07 15:51:02.418  INFO 55944 --- [           main] o.f.core.internal.command.DbMigrate      : Current version of schema `procedureDB`: << Empty Schema >>
2020-04-07 15:51:02.423  INFO 55944 --- [           main] o.f.core.internal.command.DbMigrate      : Migrating schema `procedureDB` to version 1.20200407 - Create Person Table
2020-04-07 15:51:02.479  INFO 55944 --- [           main] o.f.core.internal.command.DbMigrate      : Migrating schema `procedureDB` to version 2.20200407 - Create Person Store Procedure
2020-04-07 15:51:02.554  INFO 55944 --- [           main] o.f.core.internal.command.DbMigrate      : Successfully applied 2 migrations to schema `procedureDB` (execution time 00:00.140s)
2020-04-07 15:51:02.631  INFO 55944 --- [           main] o.hibernate.jpa.internal.util.LogHelper  : HHH000204: Processing PersistenceUnitInfo [name: default]
2020-04-07 15:51:02.728  INFO 55944 --- [           main] org.hibernate.Version                    : HHH000412: Hibernate ORM core version 5.4.12.Final
2020-04-07 15:51:02.881  INFO 55944 --- [           main] o.hibernate.annotations.common.Version   : HCANN000001: Hibernate Commons Annotations {5.1.0.Final}
2020-04-07 15:51:02.994  INFO 55944 --- [           main] org.hibernate.dialect.Dialect            : HHH000400: Using dialect: org.hibernate.dialect.MySQL8Dialect
2020-04-07 15:51:03.598  INFO 55944 --- [           main] o.h.e.t.j.p.i.JtaPlatformInitiator       : HHH000490: Using JtaPlatform implementation: [org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform]
2020-04-07 15:51:03.608  INFO 55944 --- [           main] j.LocalContainerEntityManagerFactoryBean : Initialized JPA EntityManagerFactory for persistence unit 'default'
2020-04-07 15:51:03.960  INFO 55944 --- [           main] c.h.p.SpringBootProcedureApplication     : Started SpringBootProcedureApplication in 8.016 seconds (JVM running for 8.264)
2020-04-07 15:51:04.220  INFO 55944 --- [           main] c.h.p.SpringBootProcedureApplication     : Data --> [com.hendisantika.procedure.Person@860df27a, com.hendisantika.procedure.Person@7832344f, com.hendisantika.procedure.Person@95187d4f, com.hendisantika.procedure.Person@3be3be44, com.hendisantika.procedure.Person@a3415773, com.hendisantika.procedure.Person@9616cf2e, com.hendisantika.procedure.Person@2639955b, com.hendisantika.procedure.Person@c11e4910]
2020-04-07 15:51:04.222  INFO 55944 --- [extShutdownHook] j.LocalContainerEntityManagerFactoryBean : Closing JPA EntityManagerFactory for persistence unit 'default'
2020-04-07 15:51:04.224  INFO 55944 --- [extShutdownHook] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Shutdown initiated...
2020-04-07 15:51:04.229  INFO 55944 --- [extShutdownHook] com.zaxxer.hikari.HikariDataSource       : HikariPool-1 - Shutdown completed.
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  10.810 s
[INFO] Finished at: 2020-04-07T15:51:04+07:00
[INFO] ------------------------------------------------------------------------
```

