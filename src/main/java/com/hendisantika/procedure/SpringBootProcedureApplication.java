package com.hendisantika.procedure;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootProcedureApplication {
    private static final Logger LOGGER = LogManager.getLogger(SpringBootProcedureApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SpringBootProcedureApplication.class, args);


    }

    @Bean
    CommandLineRunner initialize(PersonRepository repo) {
        return (args) -> {
            repo.addPerson("Uzumaki Naruto");
            repo.addPerson("Uchiha Sasuke");
            repo.addPerson("Sakura Haruno");
            repo.addPerson("Hatake Kakashi");
            repo.addPerson("Namikaze Minato");
            repo.addPerson("Sabaku No Gaara");
            repo.addPerson("Kankuro");
            repo.addPerson("Temari");

            LOGGER.info("Data --> {}", repo.findAll());
        };
    }
}
