package com.hendisantika.procedure;

import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-procedure
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 19/04/18
 * Time: 08.36
 * To change this template use File | Settings | File Templates.
 */
public interface PersonRepository extends CrudRepository<Person, Integer> {
    @Procedure
    void addPerson(String name);
}
